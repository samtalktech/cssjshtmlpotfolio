let navbar = document.getElementById('header-bar')
let hamburgerButton = document.getElementById('hamburger-button')
let navPane = document.getElementById('nav-list')
let firstSection = document.getElementById('section-two')
let preloader = document.getElementById('loader');


// window.addEventListener('load', offPreLoader)

function offPreLoader(){
    preloader.style.display = 'none'
}
window.onscroll = function (){test(),reveal()}

function test(){
    if (document.documentElement.scrollTop > 200) {
        navbar.classList.add('sticky-header')
    } else  {
        navbar.classList.remove('sticky-header')
    };
}

function toggleNav(e){
   
  navPane.classList.toggle('on-nav')
  

}

function reveal(){
    let sectionReveal = document.querySelectorAll('.reveals')

    for (var i = 0; i< sectionReveal.length; i++ ){
        var windowHeight = window.innerHeight;
        var revealTop = sectionReveal[i].getBoundingClientRect().top
        var revealPoint = 150; 

        if (revealTop < windowHeight - revealPoint){
            
            sectionReveal[i].classList.add('active')
        }
            else {
                sectionReveal[i].classList.remove('active')
            }
        }
    }



